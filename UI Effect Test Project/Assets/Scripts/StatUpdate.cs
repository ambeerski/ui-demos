﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;


public class StatUpdate : MonoBehaviour
{

    public TextMeshProUGUI Stat;
    Sequence moveText = DOTween.Sequence();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("mouse 0"))
        {
            statsDisplay();

        }
    }

    void statsDisplay()
    {
        Sequence Update = DOTween.Sequence();
        Update.Append(Stat.transform.DOMoveY(-60f, 0.3f).SetRelative());
        Update.AppendInterval(5);
        Update.Append(Stat.transform.DOMoveX(-500f, 0.3f));        
    }
}
