﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
using TMPro;

public class UIJuice : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public float scaleTime;
    public float scaleMax;
    public float scaleMin;

    public Button thisPotion;

    public TextMeshProUGUI StatUpdate;



    //Detect if the Cursor starts to pass over the GameObject
    public void OnPointerEnter(PointerEventData pointerEventData)
    {
        this.gameObject.transform.DOScaleX(scaleMax, scaleTime);
        this.gameObject.transform.DOScaleY(scaleMax, scaleTime);
        //Output to console the GameObject's name and the following message
        //Debug.Log("Cursor Entering " + name + " GameObject");
    }

    //Detect when Cursor leaves the GameObject
    public void OnPointerExit(PointerEventData pointerEventData)
    {
        this.gameObject.transform.DOScaleX(1f, scaleTime);
        this.gameObject.transform.DOScaleY(1f, scaleTime);
        //Output the following message with the GameObject's name
        //Debug.Log("Cursor Exiting " + name + " GameObject");
    }

    void Start()
    {
        Sequence moveText = DOTween.Sequence();

        Button b = thisPotion.GetComponent<Button>();
        b.onClick.AddListener(TaskOnClick);

        StatUpdate = gameObject.GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void TaskOnClick()
    {
        StatUpdate.text = name + "+" + Random.Range(1f, 5f);
        //Sequence Update = DOTween.Sequence();
        //Update.Append(StatUpdate.transform.DOMoveY(-60f, 0.3f).SetRelative());
        //Update.AppendInterval(5);
        //Update.Append(StatUpdate.transform.DOMoveX(-500f, 0.3f));
        Debug.Log(name + "+" + Random.Range(1, 5));
    }
}
    
